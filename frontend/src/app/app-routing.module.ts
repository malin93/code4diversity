import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  { path: '', redirectTo: 'login-tabs', pathMatch: 'full' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'login-tabs', loadChildren: './pages/login-tabs/login-tabs.module#LoginTabsPageModule' },
  { path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule' },
  { path: 'info', loadChildren: './pages/info/info.module#InfoPageModule' },
  { path: 'complete-profile', loadChildren: './pages/complete-profile/complete-profile.module#CompleteProfilePageModule' },
  { path: 'requests', loadChildren: './pages/requests/requests.module#RequestsPageModule' },
  { path: 'events', loadChildren: './pages/events/events.module#EventsPageModule' },
  { path: 'request', loadChildren: './pages/request/request.module#RequestPageModule' },
  { path: 'offers/:id', loadChildren: './pages/offers/offers.module#OffersPageModule' },
  { path: 'offers', loadChildren: './pages/offers/offers.module#OffersPageModule' },
  { path: 'public-profile/:id', loadChildren: './pages/public-profile/public-profile.module#PublicProfilePageModule' },
  { path: 'favorites', loadChildren: './pages/favorites/favorites.module#FavoritesPageModule' },
  { path: 'users', loadChildren: './pages/users/users.module#UsersPageModule' },
  { path: 'view-requests', loadChildren: './pages/view-requests/view-requests.module#ViewRequestsPageModule' },
  { path: 'view-request', loadChildren: './pages/view-request/view-request.module#ViewRequestPageModule' },
  { path: 'volunteer-home', loadChildren: './pages/volunteer-home/volunteer-home.module#VolunteerHomePageModule' },
  { path: 'admin-home', loadChildren: './pages/admin-home/admin-home.module#AdminHomePageModule' },
  { path: 'event-home', loadChildren: './pages/event-home/event-home.module#EventHomePageModule' },
  { path: 'request-info/:id', loadChildren: './pages/request-info/request-info.module#RequestInfoPageModule' },
  { path: 'volunteer-tabs', loadChildren: './pages/volunteer-tabs/volunteer-tabs.module#VolunteerTabsPageModule' },
  { path: 'volunteer-accepted', loadChildren: './pages/volunteer-accepted/volunteer-accepted.module#VolunteerAcceptedPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
