export class Request {
    beneficiaryDeleted: boolean;
    deadline: Date;
    description: String;
    id: String;
    pickupFrom: String;
    shortDesc: String;
    status: String;
    title: String;
    urgent: boolean;
    userId: String;
    when: Date;
    where: String;

}
