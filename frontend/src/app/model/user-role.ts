enum UserRole {
    Beneficiary = "Beneficiary",
    Volunteer = "Volunteer",
    Administrator = "Administrator",
    EventOrganizer = "Event Organizer",
}