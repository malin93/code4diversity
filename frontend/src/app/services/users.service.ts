import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { FirestoreService } from './firestore.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    public afs: AngularFirestore,
    private firestoreService: FirestoreService
  ) { }

  // get extra data for user that is not present in AUTH user
  getUserByUid(uid) {
    return this.firestoreService.get("users", uid);
  }

  // for admin
  getAllUsers() {
    return this.firestoreService.getAll("users");
  }

  // filter on server
  getAllUsersApproved(approved) {
    return this.firestoreService.getAllWhere("users", { field: "approved", operator: "==", value: approved });
  }

  // Search starts with, on server
  getAllUsersWhereEmail(email) {
    // return this.firestoreService.getAllWhere("users", { field: "email", operator: "==", value: email });
    const snapshot = this.afs.collection("users", ref => ref.orderBy('email', 'asc').startAt(email).endAt(email + '\uf8ff')).get();
    return snapshot.pipe(map(result => result.docs.map(doc => doc.data())));
  }

  save(user) {
    return this.firestoreService.save(user, "users", user.uid);
  }

}
