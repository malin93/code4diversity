// import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Injectable } from '@angular/core';
// import { AngularFireAuth } from "@angular/fire/auth";
// import * as firebase from 'firebase';
// import { BehaviorSubject } from 'rxjs';
// import { FirestoreService } from './firestore.service';

// TODO: this is commented for the moment because it didn't worked in Apple Safari Browser
// you can safely uncomment it and use it


// @Injectable({
//   providedIn: 'root'
// })
// export class MessagingService {

//   messaging = firebase.messaging()
//   currentMessage = new BehaviorSubject(null)

//   constructor(
//     private firestoreService: FirestoreService,
//     private http: HttpClient,
//     private afAuth: AngularFireAuth) { }


//   updateToken(token) {
//     // this.afAuth.authState.subscribe(user => {
//     //   if (!user) return;

//     //   const fcmUser = {
//     //     fcmToken: token
//     //   };
//     //   this.firestoreService.save(fcmUser, "users", user.uid)
//     // });
//   }

//   getPermission() {
//     this.messaging.requestPermission()
//       .then(() => {

//         console.log('Notification permission granted.');
//         return this.messaging.getToken();
//       })
//       .then(token => {
//         console.log(token)
//         this.updateToken(token)
//       })
//       .catch((err) => {
//         console.log('Unable to get permission to notify.', err);
//       });
//   }

//   receiveMessage() {
//     this.messaging.onMessage((payload) => {
//       console.log("Message received. ", payload);
//       this.currentMessage.next(payload)
//     });

//   }
//   sendNotification(body) {

//     const httpOptions = {
//       headers: new HttpHeaders({
//         'Content-Type': 'application/json',
//         'Authorization': 'key=AAAAYQVtEjc:APA91bF4ZQCKwlM5fIIr29Szryw1ABvpMdlhPptVMS5UUgBgwoVxQ9qiwZg0s6QQOVejGI2_Sz3sQFx5fku62NTdjutWtcaSN49KMm7cru3-XjOTrzbrEjjUjn-l4B47d8u0Sz_es9KN'
//       })
//     };

//     this.http.post("https://fcm.googleapis.com/fcm/send", body, httpOptions).subscribe(result => {
//       console.log(result);
//     });

//   }
// }