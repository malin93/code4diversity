import { Injectable } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  menuItems = [];

  menuItemsOrig = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Profile',
      url: '/profile',
      icon: 'person'
    },
    {
      title: 'Info',
      url: '/info',
      icon: 'information-circle'
    },
  ];

  constructor(
    public menuCtrl: MenuController
  ) {
    this.init();
  }

  init() {
    this.menuItems = JSON.parse(JSON.stringify(this.menuItemsOrig));
  }

  disable() {
    this.menuCtrl.enable(false);
  }

  enable() {
    this.menuCtrl.enable(true);
  }

  addMenuItem(menuItem) {
    this.menuItems.unshift(menuItem);
  }

  getMenuItems() {
    return this.menuItems;
  }

}
