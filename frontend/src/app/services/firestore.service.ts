import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  // TODO this Service should never be called directly from .page, but only from other services
  // TODO maybe make an ENUM with Collection Names so you're sure you don't misspell somewhere

  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
  ) { }

  // returns a Promise if you want to do some action after save has been executed successfully
  save(object: any, collection: String, documentId: String) {
    const objectRef: AngularFirestoreDocument<any> = this.afs.doc(`${collection}/${documentId}`);
    return objectRef.set(object, {
      merge: true
    });
  }

  get(collection, documentId) {
    const objectRef: AngularFirestoreDocument<any> = this.afs.doc(`${collection}/${documentId}`);
    return objectRef.valueChanges();
  }

  getAllWhere(collection, where) {
    const snapshot = this.afs.collection(collection, ref => ref.where(where.field, where.operator, where.value)).get();
    return snapshot.pipe(map(result => result.docs.map(doc => doc.data())));
  }

  getAllOrderBy(collection, orderBy) {
    const snapshot = this.afs.collection(collection, ref => ref.orderBy(orderBy.field, orderBy.order)).get();
    return snapshot.pipe(map(result => result.docs.map(doc => doc.data())));
  }

  getAllWhereOrderBy(collection, where, orderBy) {
    const snapshot = this.afs.collection(collection, ref => ref.where(where.field, where.operator, where.value).orderBy(orderBy.field, orderBy.order)).get();
    return snapshot.pipe(map(result => result.docs.map(doc => doc.data())));
  }

  getAll(collection) {
    const snapshot = this.afs.collection(collection).get();
    return snapshot.pipe(map(result => result.docs.map(doc => doc.data())));
  }

}
