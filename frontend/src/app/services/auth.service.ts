import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from "@angular/router";
import { auth } from 'firebase/app';
import { FirestoreService } from './firestore.service';
import { MenuService } from './menu.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userData: any;

  constructor(
    private menuService: MenuService,
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    public firestoreService: FirestoreService,
    public router: Router
  ) {
    this.afAuth.authState.subscribe(user => {
      console.log('AuthService.contructor');
      console.log(user);

      this.userData = user;
    });
  }

  // get Observable
  getAuthState() {
    return this.afAuth.authState;
  }

  signIn(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.setUserData(result.user);
        this.checkLoginConditions(result.user.uid);
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  signUp(email, password) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        console.log(result);
        this.sendVerificationMail();
        this.setUserData(result.user);
        this.router.navigate(['/complete-profile']);
      }).catch((error) => {
        window.alert(error.message);
      })
  }

  sendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification();
  }

  // NOT USED YET
  forgotPassword(passwordResetEmail) {
    return this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Password reset email sent, check your inbox.');
      }).catch((error) => {
        window.alert(error)
      })
  }

  // Sign in with Google
  googleAuth() {
    return this.authLogin(new auth.GoogleAuthProvider());
  }

  // TODO add Facebook also

  // Auth logic to run auth providers
  authLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((result) => {
        this.setUserData(result.user);
        this.checkLoginConditions(result.user.uid);
      }).catch((error) => {
        window.alert(error);
      });
  }

  checkLoginConditions(uid) {
    console.log('checkLoginConditions')

    this.firestoreService.get('users', uid).subscribe((user) => {
      if (!user.complete) {
        console.log('complete-profile')
        this.router.navigate(['/complete-profile']);
      } else {
        if (user.approved) {
          console.log(user.role);

          // correct menu items depending on Role
          switch (user.role) {
            case ("Beneficiary"): {
              this.menuService.getMenuItems().shift();
              this.menuService.addMenuItem(
                {
                  title: 'My Users',
                  url: '/users',
                  icon: 'people'
                });
              this.menuService.addMenuItem(
                {
                  title: 'Home',
                  url: '/home',
                  icon: 'home'
                });
              this.router.navigate(['/home']);
              break;
            }
            case ("Volunteer"): {
              this.menuService.getMenuItems().shift();
              this.menuService.addMenuItem(
                {
                  title: 'Home',
                  url: '/volunteer-tabs',
                  icon: 'home'
                });
              this.router.navigate(['/volunteer-tabs']);
              break;
            }
            case ("Admin"): {
              this.menuService.getMenuItems().shift();
              this.menuService.addMenuItem(
                {
                  title: 'Home',
                  url: '/admin-home',
                  icon: 'home'
                });
              this.router.navigate(['/admin-home']);
              break;
            }
            // case ("Event Organizer"): {
            //   this.menuService.getMenuItems().shift();
            //   this.menuService.addMenuItem(
            //     {
            //       title: 'Home',
            //       url: '/event-home',
            //       icon: 'home'
            //     });
            //   this.router.navigate(['/event-home']);
            //   break;
            // }
          }
        } else {
          window.alert("Your account is not active yet. Please wait for an admin to activate your account");
        }
      }
    });
  }

  // save extra User data in DB
  setUserData(user) {
    const userData: any = {
      uid: user.uid,
      email: user.email,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
    }
    return this.firestoreService.save(userData, 'users', user.uid);
  }

  // save the user in DB
  completeVerification(user) {
    return this.firestoreService.save(user, 'users', this.userData.uid);
  }

  // user should already be logged in when calling this method
  getUser() {
    return this.userData;
  }

  signOut() {
    return this.afAuth.auth.signOut().then(() => {
      this.menuService.init();
      this.router.navigate(['/login-tabs']);
    });
  }

}
