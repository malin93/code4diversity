import { Injectable } from '@angular/core';
import { FirestoreService } from './firestore.service';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(
    private authService: AuthService,
    private firestoreService: FirestoreService
  ) { }

  getById(id) {
    return this.firestoreService.get("request", id);
  }

  getAllForCurrentUser() {
    // user is assumed to be already logged in
    return this.firestoreService.getAllWhere("request", { field: "userId", operator: "==", value: this.authService.getUser().uid });
  }

  getAllAvailable() {
    // TODO fix date check (there is a problem with the date as it's not seen as Date, but String, it's easily fixable)

    // decomment this if you fix it
    // return this.firestoreService.getAllWhereOrderBy("request", {field: "deadline", operator: ">=", value: Date.now()}, { field: "deadline", order: "asc" });

    // TODO remove 'friends only for users that are not your friends'
    return this.firestoreService.getAllOrderBy("request", { field: "deadline", order: "asc" });
  }

  getAll() {
    return this.firestoreService.getAllOrderBy("request", { field: "deadline", order: "asc" });
  }

  save(request) {
    return this.firestoreService.save(request, "request", request.id);
  }

}
