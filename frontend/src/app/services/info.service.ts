import { Injectable } from '@angular/core';
import { FirestoreService } from './firestore.service';

@Injectable({
  providedIn: 'root'
})
export class InfoService {

  constructor(
    private firestoreService: FirestoreService
  ) { }

  // there is only 1 Info page for all users, so we create just one
  get() {
    return this.firestoreService.get("info", "1");
  }

  // TODO if you want different Infos for different users, you can add more here and filter them in Get by Role
  save(info) {
    return this.firestoreService.save(info, "info", "1");
  }

}
