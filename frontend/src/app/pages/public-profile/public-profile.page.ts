import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-public-profile',
  templateUrl: './public-profile.page.html',
  styleUrls: ['./public-profile.page.scss'],
})
export class PublicProfilePage implements OnInit {

  currentUser;

  user;

  favorite;

  blocked;

  hideFavoriteBlocked;

  constructor(
    private authService: AuthService,
    private usersService: UsersService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    // you need to wait for the Login to finish before getting the user from FireStore
    this.authService.getAuthState().subscribe(user => {
      if (user) {
        this.usersService.getUserByUid(user.uid).subscribe(user => {
          if (user) {
            this.currentUser = user;

            // when passing params from one page to the other you can get it like this
            const userId = this.route.snapshot.paramMap.get('id');
            this.hideFavoriteBlocked = this.route.snapshot.queryParams['hideFavoriteBlocked'];

            this.usersService.getUserByUid(userId).subscribe(user => {
              if (user) {
                this.user = user;

                // init the Favorite & Blocked toggles correctly
                if (this.currentUser.myFriends) {
                  // myFriends is an array
                  this.favorite = this.currentUser.myFriends.map(friend => friend.uid).includes(user.uid);
                }
                if (this.currentUser.myBlocked) {
                  // myBlocked is an array
                  this.blocked = this.currentUser.myBlocked.map(blocked => blocked.uid).includes(user.uid);
                }
              }
            });
          }
        });
      }
    });
  }

  toggleFavorite() {
    if (this.favorite) {
      // if array is null, initialize it with empty array
      if (!this.currentUser.myFriends) {
        this.currentUser.myFriends = [];
      }
      // we add here all details we require when reading myFriends in any page (as Joins are not easy/possible in NoSQL)
      this.currentUser.myFriends.push({
        uid: this.user.uid,
        email: this.user.email,
        photoURL: this.user.photoURL
      });
    } else {
      // if array is null, initialize it with empty array
      if (!this.currentUser.myFriends) {
        this.currentUser.myFriends = [];
      }
      // remove the user from the array
      this.currentUser.myFriends = this.currentUser.myFriends.filter(obj => obj.uid != this.user.uid);
    }
    this.usersService.save(this.currentUser);
  }

  // same comments as in toggleFavorite()
  toggleBlocked() {
    if (this.blocked) {
      if (!this.currentUser.myBlocked) {
        this.currentUser.myBlocked = [];
      }
      this.currentUser.myBlocked.push({
        uid: this.user.uid,
        email: this.user.email,
        photoURL: this.user.photoURL
      });
    } else {
      if (!this.currentUser.myBlocked) {
        this.currentUser.myBlocked = [];
      }
      this.currentUser.myBlocked = this.currentUser.myBlocked.filter(obj => obj.uid != this.user.uid);
    }
    this.usersService.save(this.currentUser);
  }

}
