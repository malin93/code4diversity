import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { InfoService } from 'src/app/services/info.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  info;

  user;

  constructor(
    private authService: AuthService,
    private usersService: UsersService,
    private router: Router,
    private infoService: InfoService
  ) { }

  ngOnInit() {
    // you need to wait for the Login to finish before getting the user from FireStore
    this.authService.getAuthState().subscribe(user => {
      console.log(user)

      // Login has successfully been done
      if (user) {
        this.usersService.getUserByUid(user.uid).subscribe(user => {
          console.log(user)
          if (user) {
            this.user = user;
          }
        });
      }
    });

    this.infoService.get().subscribe(info => this.info = info.value);
  }

  isSaveVisible() {
    return this.user && "Admin" == this.user.role;
  }

  // this button is available only for admins, so we can safely navigate to admin home
  onSubmit() {
    this.infoService.save({ value: this.info }).then(res => {
      this.router.navigate(['/admin-home']);
    });
  }

}
