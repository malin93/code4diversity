import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage,
        children: [
          {
            path: 'requests',
            loadChildren: '../requests/requests.module#RequestsPageModule'
          }, {
            path: 'events',
            loadChildren: '../events/events.module#EventsPageModule'
          }
        ]
      }
    ])
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
