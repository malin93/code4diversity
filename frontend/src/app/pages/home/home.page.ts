import { Component, ViewChild } from '@angular/core';
import { MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  @ViewChild('homeTabs') homeTabs;

  constructor(
    private menuService: MenuService
  ) { }

  ionViewWillEnter() {
    this.homeTabs.select('requests');
    this.menuService.enable();
  }

}
