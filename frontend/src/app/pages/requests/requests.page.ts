import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { RequestsService } from 'src/app/services/requests.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.page.html',
  styleUrls: ['./requests.page.scss'],
})
export class RequestsPage {

  requests;

  constructor(
    private authService: AuthService,
    private requestsService: RequestsService,
    private router: Router
  ) { }

  ionViewWillEnter() {
    console.log('RequestsPage.ionViewWillEnter');
    this.authService.getAuthState().subscribe(user => {
      if (user) {
        // we need to make sure we have the current user when we do this query
        this.requestsService.getAllForCurrentUser().subscribe((results: any[]) => {
          // hide the deleted ones
          this.requests = results.filter(result => !result.beneficiaryDeleted);
        });
      }
    })
  }

  getStatusColor(status) {
    switch (status) {
      case "Not posted yet":
        return "blue";
      case "Waiting for Volunteers":
        return "orange";
      case "Volunteer Selected":
        return "green";
      case "Finished":
        return "gray";
      default:
        return "black";
    }
  }

  add() {
    this.router.navigate(['/request']);
  }

  // not used yet
  edit(request) {
    this.router.navigate(['/request', request.id]);
  }

  // not used yet
  cancel(request) {
    // TODO replace window.alert with some other information method
    window.alert('Are you sure you want to cancel ' + request.title);
    request.status = "Not posted yet";
  }

  delete(request) {
    window.alert('Are you sure you want to delete ' + request.title);
    // we don't delete the request for good, because maybe the request has a selected Volunteer that still wants to see this request
    // we just hide it for the beneficiary
    request.beneficiaryDeleted = true;
    this.requests = this.requests.filter(obj => obj.id != request.id);
    this.requestsService.save(request);
  }

  markAsCompleted(request) {
    request.status = "Finished";
    this.requestsService.save(request);
  }

  viewOffers(request) {
    this.router.navigate(['/offers', request.id]);
  }

  // not used yet
  isViewOffersVisible(request) {
    return request.applyingVolunteerUserIds && request.applyingVolunteerUserIds.length > 0;
  }

  // not used yet
  isEditVisible(request) {
    if (request) {
      return request.status == "Not posted yet" || request.status == "Waiting for Volunteers";
    } else {
      return false;
    }
  }

  // not used yet
  isCancelVisible(request) {
    if (request) {
      return request.status == "Waiting for Volunteers" || request.status == "Volunteer Selected";
    } else {
      return false;
    }
  }

  // not used yet
  isRemoveVisible(request) {
    if (request) {
      return request.status == "Not posted yet" || request.status == "Finished";
    } else {
      return false;
    }
  }

}
