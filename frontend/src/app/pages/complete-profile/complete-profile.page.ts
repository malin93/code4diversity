import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-complete-profile',
  templateUrl: './complete-profile.page.html',
  styleUrls: ['./complete-profile.page.scss'],
})
export class CompleteProfilePage {

  profileForm: FormGroup;

  returnUrl;

  submitted;

  loading;

  constructor(
    private formBuilder: FormBuilder,
    private navCtrl: NavController,
    private authService: AuthService) {

    // init form
    // TODO for Social SignIn maybe prepopulate available values
    this.profileForm = this.formBuilder.group({
      name: ['', Validators.required],
      phone: ['', Validators.required],
      bio: ['', Validators.required],
      role: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.profileForm.controls; }

  onSubmit() {
    if (this.profileForm.invalid) {
      // TODO display some validation messages to the user
      return;
    }

    const user = {
      name: this.f.name.value,
      phone: this.f.phone.value,
      bio: this.f.bio.value,
      role: this.f.role.value,
      approved: false,
      complete: true
    }

    this.authService.completeVerification(user).then(() => {
      this.navCtrl.navigateRoot("/");
    });
  }

}
