import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirestoreService } from 'src/app/services/firestore.service';



@Component({
  selector: 'app-request-info',
  templateUrl: './request-info.page.html',
  styleUrls: ['./request-info.page.scss'],
})
export class RequestInfoPage implements OnInit {
  requestId;
  request;
  constructor(
    private route: ActivatedRoute,
    private firestoreService: FirestoreService
  ) {
    this.requestId = this.route.snapshot.paramMap.get("id");
    this.firestoreService.get("request", this.requestId).subscribe(result => {
      console.log(result);
      this.request = result;
    });
  }

  ngOnInit() {

  }

}
