import { Component } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
// TODO add !photo upload! to User Details
export class ProfilePage {

  user: any;

  profileForm: FormGroup;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    public afs: AngularFirestore) { }

  ionViewDidEnter() {
    const firebaseUser = this.authService.getUser();

    // if user not already Loaded
    if (firebaseUser) {
      const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${firebaseUser.uid}`);

      // get user from FireStore
      userRef.valueChanges().subscribe(data => {
        console.log(data);

        // pre-populate form
        this.profileForm = this.formBuilder.group({
          name: [data.name, Validators.required],
          bio: [data.bio, Validators.required],
          phone: [data.phone, Validators.required],
        });
        this.user = data;
      });
    }
  }

  onSubmit() {
    if (this.profileForm.invalid) {
      // TODO display some validation messages to the user
      return;
    }

    this.user.name = this.profileForm.controls.name.value;
    this.user.bio = this.profileForm.controls.bio.value;
    this.user.phone = this.profileForm.controls.phone.value;

    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${this.user.uid}`);

    // save the changes
    userRef.set(this.user, {
      merge: true
    }).then(() => {
      // exit screen to the correct Home depending on the Role
      switch (this.user.role) {
        case ("Beneficiary"): {
          this.router.navigate(['/home']);
          return;
        }
        case ("Volunteer"): {
          this.router.navigate(['/volunteer-home']);
          return;
        }
        case ("Admin"): {
          this.router.navigate(['/admin-home']);
          return;
        }
      }

      this.router.navigate(['/home']);
    });
  }

}
