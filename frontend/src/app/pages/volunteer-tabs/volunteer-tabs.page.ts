import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-volunteer-tabs',
  templateUrl: './volunteer-tabs.page.html',
  styleUrls: ['./volunteer-tabs.page.scss'],
})
export class VolunteerTabsPage implements OnInit {

  @ViewChild('volunteerTabs') volunteerTabs;

  constructor(
    private menuService: MenuService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    // select first tab
    this.volunteerTabs.select('volunteer-home');
    this.menuService.enable();
  }

}
