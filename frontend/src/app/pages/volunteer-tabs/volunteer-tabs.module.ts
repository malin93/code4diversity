import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VolunteerTabsPage } from './volunteer-tabs.page';

const routes: Routes = [
  {
    path: '',
    component: VolunteerTabsPage,
    children: [
      {
        path: 'volunteer-home',
        loadChildren: '../volunteer-home/volunteer-home.module#VolunteerHomePageModule'
      }, {
        path: 'volunteer-accepted',
        loadChildren: '../volunteer-accepted/volunteer-accepted.module#VolunteerAcceptedPageModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VolunteerTabsPage]
})
export class VolunteerTabsPageModule { }
