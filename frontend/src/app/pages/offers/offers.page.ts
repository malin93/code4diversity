import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { RequestsService } from 'src/app/services/requests.service';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit {

  request;

  constructor(
    private requestsService: RequestsService,
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private router: Router
  ) { }

  ngOnInit() {
    // get current request
    this.requestsService.getById(this.route.snapshot.paramMap.get("id")).subscribe(request => {
      this.request = request;
      console.log(request);
    });
  }

  // uid is the UID of the User that was selected by the Beneficiary
  selectVolunteer(uid) {
    this.request.acceptedVolunteer = uid;
    this.request.status = "Volunteer Selected";
    this.requestsService.save(this.request).then(obj => {
      //
      this.navCtrl.navigateBack('/home/requests');
    });
  }

  viewProfile(uid) {
    this.router.navigate(['/public-profile', uid]);
  }

}
