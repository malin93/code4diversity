import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { FirestoreService } from 'src/app/services/firestore.service';
import { v4 as uuid } from 'uuid';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.page.html',
  styleUrls: ['./request.page.scss'],
})
export class RequestPage implements OnInit {

  requestForm: FormGroup;

  request;

  edit;

  constructor(
    private authService: AuthService,
    private firestoreService: FirestoreService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private navCtrl: NavController) { }

  ngOnInit() {
    // this is for edit only, but for the moment the EDIT BUTTON is not used
    const requestId = this.route.snapshot.paramMap.get("id");
    console.log(requestId);

    if (requestId) {
      this.edit = true;
      this.firestoreService.get("request", requestId).subscribe(result => {
        this.request = result;

        this.requestForm = this.formBuilder.group({
          urgent: [this.request.urgent, Validators.required],
          friendsOnly: [this.request.friendsOnly, Validators.required],
          title: [this.request.title, Validators.required],
          shortDesc: [this.request.shortDesc, Validators.required],
          description: [this.request.description, Validators.required],
          when: [this.request.when, Validators.required],
          where: [this.request.where, Validators.required],
          pickupFrom: [this.request.pickupFrom, Validators.required],
          deadline: [this.request.deadline, Validators.required]
        });
      });
    } else {
      this.edit = false;
      this.request = {};

      this.requestForm = this.formBuilder.group({
        urgent: [false, Validators.required],
        friendsOnly: [false, Validators.required],
        title: ['', Validators.required],
        shortDesc: ['', Validators.required],
        description: ['', Validators.required],
        when: ['', Validators.required],
        where: ['', Validators.required],
        pickupFrom: ['', Validators.required],
        deadline: ['', Validators.required]
      });
    }
  }

  send() {
    if (this.requestForm.invalid) {
      let errors = [];
      // TODO copy this validation handling to all other screen where xxxForm.invalid is checked
      Object.entries(this.requestForm.controls).forEach(entry => {
        if (entry[1].invalid) {
          errors.push(this.getErrorMessage(entry[0], entry[1].errors));
        }
      });
      window.alert(errors);
      return;
    }

    this.mapToRequest(this.requestForm.controls);
    const uid = this.authService.getUser().uid;
    this.firestoreService.get("users", uid).subscribe(user => {
      this.request.name = user.name;
      this.request.email = user.email;
      this.request.uid = user.uid;
      this.request.phoneNumber = user.phone;
      this.request.bio = user.bio;
      // this.request.fcmToken = user.fcmToken;
      this.firestoreService.save(this.request, "request", this.request.id).then(result => {
        this.navCtrl.navigateBack('/home/requests');
      });
    });

  }

  mapToRequest(controls) {
    if (!this.request.id) {
      // generate a random unique id
      this.request.id = uuid();
    }
    if (!this.request.status) {
      // SAVE makes it immediately available for Volunteers
      // TODO you could add SAVE FOR LATER button
      this.request.status = "Waiting for Volunteers";
    }
    this.request.urgent = controls.urgent.value;
    this.request.friendsOnly = controls.friendsOnly.value;
    this.request.title = controls.title.value;
    this.request.shortDesc = controls.shortDesc.value;
    this.request.description = controls.description.value;
    this.request.when = controls.when.value;
    this.request.where = controls.where.value;
    this.request.pickupFrom = controls.pickupFrom.value;
    this.request.deadline = controls.deadline.value;
    this.request.beneficiaryDeleted = false;
    if (!this.request.userId) {
      // here the user si already loaded
      this.request.userId = this.authService.getUser().uid;
    }
  }

  getTitle() {
    if (this.edit) { // TODO not used now, but you can easily switch it on
      return "Edit Request";
    } else {
      return "New Request";
    }
  }

  getErrorMessage(name, errors) {
    // TODO here I have only 'required' checks, but you can implemented more complex validation as well
    if (errors.required == true) {
      return name + " is required!";
    }
    return "";
  }

}
