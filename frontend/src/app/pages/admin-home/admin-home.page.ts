import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { MenuService } from 'src/app/services/menu.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.page.html',
  styleUrls: ['./admin-home.page.scss'],
})
export class AdminHomePage implements OnInit {

  segment;

  filter;

  users;

  constructor(
    private menuService: MenuService,
    private usersService: UsersService,
    private router: Router
  ) { }

  ngOnInit() {
    this.usersService.getAllUsersApproved(false).subscribe(users => {
      this.users = users;
    });
  }

  ionViewWillEnter() {
    // menu is disabled for Login/Registered, so we need to re-enable it here
    this.menuService.enable();
  }

  getIcon(role) {
    switch (role) {
      case "Beneficiary":
        return "eye-off";
      case "Volunteer":
        return "body";
      case "Admin":
        return "construct";
    }
  }

  viewUser(user) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // within Public Profile there are 2 toggles: Friend, Block
        // we want those displayed only for Beneficiary from Offers page only
        hideFavoriteBlocked: true
      }
    };
    this.router.navigate(['/public-profile', user.uid], navigationExtras);
  }

  segmentChanged(value) {
    console.log('AdminHomePage.segmentChanged');

    this.segment = value;

    if (this.filter && this.filter != "") {
      this.onSearch();
    } else {
      if ("all" == this.segment) {
        this.usersService.getAllUsers().subscribe(users => {
          this.users = users;
        });
      } else {
        this.usersService.getAllUsersApproved("approved" == this.segment).subscribe(users => {
          this.users = users;
        });
      }
    }
  }

  onSearch() {
    console.log('AdminHomePage.onSearch');

    // this gets current user as well, it should not be possible to lock yourself out
    // although you can fix it in FireBase FireStore
    this.usersService.getAllUsersWhereEmail(this.filter).subscribe(users => {
      this.users = users.filter(result => {
        if ("all" == this.segment) {
          return true;
        } else {
          return "approved" == this.segment ? result.approved : !result.approved;
        }
      });
    });
  }

  approveToggle(user) {
    console.log('AdminHomePage.approveToggle');

    user.approved = !user.approved;
    this.usersService.save(user);
  }

}
