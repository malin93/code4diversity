import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-login-tabs',
  templateUrl: './login-tabs.page.html',
  styleUrls: ['./login-tabs.page.scss'],
})
export class LoginTabsPage implements OnInit {

  @ViewChild('loginTabs') loginTabs;

  constructor(private menuService: MenuService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    // we select Login tab as being active by default
    this.loginTabs.select('login');

    // we disable menu for Login/Register
    this.menuService.disable();
  }

}
