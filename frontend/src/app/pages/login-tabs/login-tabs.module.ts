import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoginTabsPage } from './login-tabs.page';

const routes: Routes = [
  {
    path: '',
    component: LoginTabsPage,
    children: [
      {
        path: 'login',
        loadChildren: '../login/login.module#LoginPageModule'
      }, {
        path: 'register',
        loadChildren: '../register/register.module#RegisterPageModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LoginTabsPage]
})
export class LoginTabsPageModule {}
