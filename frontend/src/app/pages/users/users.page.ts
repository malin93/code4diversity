import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {

  currentUser;

  friends;

  blocked;

  constructor(
    private authService: AuthService,
    private usersService: UsersService,
    private router: Router
  ) { }

  ngOnInit() {
    this.authService.getAuthState().subscribe(user => {
      if (user) {
        this.usersService.getUserByUid(user.uid).subscribe(user => {
          if (user) {
            this.currentUser = user;
            // we save friends and blocked in 2 different lists
            this.friends = user.myFriends;
            this.blocked = user.myBlocked;
          }
        });
      }
    });
  }

  viewUser(user) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        hideFavoriteBlocked: true
      }
    };
    this.router.navigate(['/public-profile', user.uid], navigationExtras);
  }

  refreshUser() {
    this.usersService.getUserByUid(this.currentUser.uid).subscribe(user => {
      if (user) {
        this.currentUser = user;
        this.friends = user.myFriends;
        this.blocked = user.myBlocked;
      }
    });
  }

  toggleBlocedkUser(user) {
    this.currentUser.myBlocked = this.currentUser.myBlocked.filter(blocked => blocked.uid != user.uid);
    this.usersService.save(this.currentUser).then(obj => this.refreshUser());
  }

  toggleMakeFriend(user) {
    this.currentUser.myFriends = this.currentUser.myFriends.filter(friend => friend.uid != user.uid);
    this.usersService.save(this.currentUser).then(obj => this.refreshUser());
  }

}
