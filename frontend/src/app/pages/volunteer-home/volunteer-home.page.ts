import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { RequestsService } from 'src/app/services/requests.service';
import { UsersService } from 'src/app/services/users.service';
// import { MessagingService } from 'src/app/services/messaging.service';

@Component({
  selector: 'app-volunteer-home',
  templateUrl: './volunteer-home.page.html',
  styleUrls: ['./volunteer-home.page.scss'],
})
export class VolunteerHomePage implements OnInit {

  filter;

  requests;

  segment;

  user;

  constructor(
    private router: Router,
    private authService: AuthService,
    private usersService: UsersService,
    private requestsService: RequestsService,
    // private msgService: MessagingService // TODO decomment this for Firebase Notifications!
  ) {

  }

  ngOnInit() {
  }

  ionViewWillEnter() {

    this.authService.getAuthState().subscribe(user => {
      if (user) {
        this.usersService.getUserByUid(user.uid).subscribe(user => {
          if (user) {
            this.user = user;
            this.requestsService.getAllAvailable().subscribe((results: any[]) => {
              console.log('urgent');
              console.log(results);
              // get all urgent first (including where current user is Pending or Accepted)
              this.requests = results.filter(result => result.urgent);
            });
          }
        });
      }
    });
  }

  // view Beneficiary details, without being able to block/friend him
  viewUser(uid) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        hideFavoriteBlocked: true
      }
    };
    this.router.navigate(['/public-profile', uid], navigationExtras);
  }

  showMore(request) {
    request.showMore = true;
  }

  segmentChanged(value) {
    this.segment = value;

    if ("urgent" == this.segment) {
      this.requestsService.getAllAvailable().subscribe((results: any[]) => {
        console.log('urgent');
        console.log(results);
        this.requests = results.filter(result => result.urgent);
      });
    }

    if ("noVolunteers" == this.segment) {
      this.requestsService.getAllAvailable().subscribe((results: any[]) => {
        console.log('noVolunteers');
        console.log(results);
        // get all where there are no volunteers (implicitly there won't be any accepted volunteer)
        this.requests = results.filter(result => !result.volunteers || result.volunteers.length == 0);
      });
    }

    if ("all" == this.segment) {
      this.requestsService.getAllAvailable().subscribe((results: any[]) => {
        console.log('all');
        console.log(results);
        // get all not expired
        this.requests = results;
      });
    }
  }

  showRequest(request) {
    this.router.navigate(['/request-info', request.id]);
  }

  acceptRequest(request) {
    const index = this.requests.indexOf(request, 0);
    if (index > -1) {
      this.requests.splice(index, 1);
    }
    if (request.volunteers != undefined) {
      request.volunteers.push({
        uid: this.user.uid,
        name: this.user.name,
        email: this.user.email,
        photoURL: this.user.photoURL,
        bio: this.user.bio
      });
    } else {
      request.volunteers = [{
        uid: this.user.uid,
        name: this.user.name,
        email: this.user.email,
        photoURL: this.user.photoURL,
        bio: this.user.bio
      }];
    }
    const body = {
      notification: {
        title: this.user.name + " accepted your " + request.title + " request.",
        body: "Open app to approve volunteer",
        click_action: "https://code4diversity-7985f.firebaseapp.com",
      },
      to: request.fcmToken
    }
    // this.msgService.sendNotification(body);

    request.showMore = false;
    this.requestsService.save(request);
  }

}
