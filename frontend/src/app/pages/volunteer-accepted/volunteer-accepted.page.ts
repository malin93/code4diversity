import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { RequestsService } from 'src/app/services/requests.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-volunteer-accepted',
  templateUrl: './volunteer-accepted.page.html',
  styleUrls: ['./volunteer-accepted.page.scss'],
})
export class VolunteerAcceptedPage implements OnInit {

  user;
  requests;
  segment;

  constructor(
    private authService: AuthService,
    private requestsService: RequestsService,
    private usersService: UsersService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    // get current user from AUTH
    this.authService.getAuthState().subscribe(user => {
      if (user) {
        // get current user from DB
        this.usersService.getUserByUid(user.uid).subscribe(user => {
          if (user) {
            this.user = user;
            // get current user's requests! where he was accepted
            this.requestsService.getAllAvailable().subscribe((results: any[]) => {
              this.requests = results.filter(result => result.acceptedVolunteer == user.uid);
              console.log(this.requests);
            });
          }
        });
      }
    });
  }

  markAsCompleted(request) {
    request.status = "Finished";
    this.requestsService.save(request);
  }

  segmentChanged(value) {
    this.segment = value;

    if ("accepted" == this.segment) {
      this.requestsService.getAllAvailable().subscribe((results: any[]) => {
        console.log('urgent');
        console.log(results);
        // he's the accepted volunteer
        this.requests = results.filter(result => result.acceptedVolunteer == this.user.uid);
      });
    }

    if ("pending" == this.segment) {
      this.requestsService.getAllAvailable().subscribe((results: any[]) => {
        console.log('noVolunteers');
        console.log(results);
        // there's NO accepted volunteer and he is a Pending Volunteer
        this.requests = results.filter(result => result.volunteers && result.volunteers.map(volunteer => volunteer.uid).includes(this.user.uid) && !result.acceptedVolunteer);
      });
    }

    if ("friends" == this.segment) {
      this.requestsService.getAllAvailable().subscribe((results: any[]) => {
        this.requests = results.filter(result =>
          // the Request is created by one of his friends, doesn't matter if he is Pending/Accepted or not at all
          this.user.myFriends && this.user.myFriends.map(friend => friend.uid).includes(result.uid)
        );
      });
    }

  }

}
