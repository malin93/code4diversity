import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VolunteerAcceptedPage } from './volunteer-accepted.page';

const routes: Routes = [
  {
    path: '',
    component: VolunteerAcceptedPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VolunteerAcceptedPage]
})
export class VolunteerAcceptedPageModule {}
