import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { MenuService } from './services/menu.service';
// import { MessagingService } from './services/messaging.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public appPages = [];
  user: any;
  message;
  constructor(
    private menuService: MenuService,
    private authService: AuthService,
    // private msgService: MessagingService,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    console.log('initialize app');

    this.authService.afAuth.authState.subscribe(user => {
      if (user) {
        this.appPages = this.menuService.getMenuItems();
        this.user = user;
      } else {
        this.router.navigate(['/']);
      }
    });
  }

  ngOnInit() {
    // this.msgService.getPermission()
    // this.msgService.receiveMessage()
    // this.message = this.msgService.currentMessage.subscribe(data => {
    //   console.log(data);
    //   if (data) {
    //     window.alert(data.notification.title);
    //   }

    // });
  }

  logout() {
    this.authService.signOut();
  }
}
